package com.example.fridaythree

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fridaythree.databinding.FragmentOpenedBinding

class OpenedFragment : Fragment() {

    private var _binding: FragmentOpenedBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOpenedBinding.inflate(inflater, container, false)
        getImage()
        return binding.root
    }

    private fun getImage(){
        val img = arguments?.getInt("item", R.mipmap.nothing)
        if (img != null) {
            binding.imVOpened.setImageResource(img)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}