package com.example.fridaythree

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.fridaythree.databinding.FragmentMainBinding


class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!


    lateinit var adapter: ViewPagerAdapter

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {

        activity?.let {
            adapter = ViewPagerAdapter(it, object : OnItemListener {
                override fun itemOnClick(position: Int) {
                    openImage(position)
                }

            })
        }

        binding.viewPager.adapter = adapter
    }

    private fun openImage(position: Int){
        val bundle = bundleOf("item" to ItemsEnum.values()[position].image)
        findNavController().navigate(R.id.action_mainFragment_to_openedFragment, bundle)

    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}