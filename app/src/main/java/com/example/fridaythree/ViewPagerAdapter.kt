package com.example.fridaythree

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter

class ViewPagerAdapter(
    private val context: Context,
    private val onItemListener: OnItemListener
) :
    PagerAdapter() {

    lateinit var layoutInflater: LayoutInflater

    override fun getCount() = ItemsEnum.values().size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.item_layout, container, false)

        val image = view.findViewById<ImageView>(R.id.imVPhoto)
        image.setImageResource(ItemsEnum.values()[position].image)
        container.addView(view, 0)

        view.setOnClickListener{
            onItemListener.itemOnClick(position)
        }

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}