package com.example.fridaythree

interface OnItemListener {
    fun itemOnClick(position: Int)
}