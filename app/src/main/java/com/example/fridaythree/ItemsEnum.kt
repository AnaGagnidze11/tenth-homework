package com.example.fridaythree

enum class ItemsEnum(val image: Int) {
    CHERRY(R.mipmap.cherry),
    FIRST_CAT(R.mipmap.first_cat),
    GIRLS(R.mipmap.girls),
    SECOND_CAT(R.mipmap.second_cat),
    STRAWBERRY(R.mipmap.strawberry),
    THIRD_CAT(R.mipmap.third_cat),
    SUN(R.mipmap.sun),
    FORTH_CAT(R.mipmap.forth_cat)
}